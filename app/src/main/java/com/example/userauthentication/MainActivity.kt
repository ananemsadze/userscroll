package com.example.userauthentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.security.KeyStore
import kotlin.math.log

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        texts()


    }



    private fun opensecondactivity(){
        val name = Name.text.toString()
        val info = Info.text.toString()
        val intent = Intent(this, ForUser::class.java)
        intent.putExtra("name", name)
        intent.putExtra("info", info)
        startActivity(intent)
        Toast.makeText(this, "Successful Log in", Toast.LENGTH_SHORT).show()}

    private fun texts() {

        logbutton.setOnClickListener() {
            if (EmailEnter.text.toString().isNotEmpty() && PasswordEnter.text.toString().isNotEmpty()) {
                emailchecker()
                if (Name.text.toString().isNotEmpty() && Info.text.toString().isNotEmpty()) {
                    opensecondactivity()
                }


            } else if (Name.text.toString().isEmpty() || Info.text.toString().isEmpty()) {

                Toast.makeText(this, "Please fill all fields to log in", Toast.LENGTH_SHORT).show()

            }  else if (EmailEnter.text.toString().isEmpty() || PasswordEnter.text.toString().isEmpty()) {

            Toast.makeText(this, "Please fill all fields to log in", Toast.LENGTH_SHORT).show()
            }
        }
    }


    private fun emailchecker() {
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(EmailEnter.text.toString()).matches())

        else{ EmailEnter.setError("Email format is not correct")

        }
    }
}

