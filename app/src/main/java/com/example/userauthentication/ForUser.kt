package com.example.userauthentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_for_user.*

class ForUser : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_for_user)
        init()
        images()
    }

    private fun init() {


        val name = intent.extras?.getString("name", "")
        val info = intent.extras?.getString("info", "")



        NameView.text = name
        InfoView.text = info

        }

    private fun images(){
        Glide.with(this)
                .load("https://stat.ameba.jp/user_images/20190330/13/uyu-jin-1204/0e/9e/j/o0750093914381403859.jpg?cpd=800")
                .placeholder(R.color.black)
                .error(R.color.black)
                .into(Profile);
        Glide.with(this)
                .load("https://static.boredpanda.com/blog/wp-content/uploads/2018/10/DIkW45bUQAAJWbX-5bce2a3fbe311__700.jpg")
                .placeholder(R.color.black)
                .error(R.color.black)
                .into(Image1);

        Glide.with(this)
                .load("https://64.media.tumblr.com/1f34b2246f65826bc27e9ea95d80d32e/tumblr_osatx2OAkR1tk7ffbo3_1280.jpg")
                .placeholder(R.color.black)
                .error(R.color.black)
                .into(Image2);

        Glide.with(this)
                .load("https://data.whicdn.com/images/319760727/original.jpg")
                .placeholder(R.color.black)
                .error(R.color.black)
                .into(Image3);
        Glide.with(this)
                .load("https://static.boredpanda.com/blog/wp-content/uploads/2018/10/DnmR60ZUwAA_x1E-5bce2a757de21__700.jpg")
                .placeholder(R.color.black)
                .error(R.color.black)
                .into(Image4);

        Glide.with(this)
                .load("https://pm1.narvii.com/6486/6254dc74772a41520804dc429f4b1bc490f2542d_hq.jpg")
                .placeholder(R.color.black)
                .error(R.color.black)
                .into(Image5);

        Glide.with(this)
                .load("https://data.whicdn.com/images/319760652/original.jpg")
                .placeholder(R.color.black)
                .error(R.color.black)
                .into(Image6);

        Glide.with(this)
                .load("https://static.boredpanda.com/blog/wp-content/uploads/2018/10/DIkW45hV4AA8q1o-5bce2a419fa44__700.jpg")
                .placeholder(R.color.black)
                .error(R.color.black)
                .into(Image7);

    }




    }


